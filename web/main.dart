// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';

class BanUI {
  List<Element> masu_;

  BanUI() {
    masu_ = new List(8*8);
    int i = 0;
    Element e;

    e = querySelector('#a1');
    masu_[i] = e
        ..onClick.listen(clicka1);
//        ..onMouseOut.listen(hoverouta1)
//        ..onMouseOver.listen(hoverina1);
    ++i;
    e = querySelector('#a2');
    masu_[i] = e
        ..onClick.listen(clicka2);
//        ..onMouseOut.listen(hoverouta2)
//        ..onMouseOver.listen(hoverina2);
    ++i;
    e = querySelector('#a3');
    masu_[i] = e
        ..onClick.listen(clicka3);
//        ..onMouseOut.listen(hoverouta3)
//        ..onMouseOver.listen(hoverina3);
    ++i;
    e = querySelector('#a4');
    masu_[i] = e
        ..onClick.listen(clicka4);
//        ..onMouseOut.listen(hoverouta4)
//        ..onMouseOver.listen(hoverina4);
    ++i;
    e = querySelector('#a5');
    masu_[i] = e
        ..onClick.listen(clicka5);
//        ..onMouseOut.listen(hoverouta5)
//        ..onMouseOver.listen(hoverina5);
    ++i;
    e = querySelector('#a6');
    masu_[i] = e
        ..onClick.listen(clicka6);
//        ..onMouseOut.listen(hoverouta6)
//        ..onMouseOver.listen(hoverina6);
    ++i;
    e = querySelector('#a7');
    masu_[i] = e
        ..onClick.listen(clicka7);
//        ..onMouseOut.listen(hoverouta7)
//        ..onMouseOver.listen(hoverina7);
    ++i;
    e = querySelector('#a8');
    masu_[i] = e
        ..onClick.listen(clicka8);
//        ..onMouseOut.listen(hoverouta8)
//        ..onMouseOver.listen(hoverina8);
    ++i;
  }
  void clicka1(MouseEvent e) {absclick(0, 0);}
  void clicka2(MouseEvent e) {absclick(1, 0);}
  void clicka3(MouseEvent e) {absclick(2, 0);}
  void clicka4(MouseEvent e) {absclick(3, 0);}
  void clicka5(MouseEvent e) {absclick(4, 0);}
  void clicka6(MouseEvent e) {absclick(5, 0);}
  void clicka7(MouseEvent e) {absclick(6, 0);}
  void clicka8(MouseEvent e) {absclick(7, 0);}
  void clickb1(MouseEvent e) {absclick(0, 1);}
  void clickb2(MouseEvent e) {absclick(1, 1);}
  void clickb3(MouseEvent e) {absclick(2, 1);}
  void clickb4(MouseEvent e) {absclick(3, 1);}
  void clickb5(MouseEvent e) {absclick(4, 1);}
  void clickb6(MouseEvent e) {absclick(5, 1);}
  void clickb7(MouseEvent e) {absclick(6, 1);}
  void clickb8(MouseEvent e) {absclick(7, 1);}

  void absclick(int x, int y) {
    int i = x+y*8;
    masu_[i].text = "clicked";
  }
}

void main() {
  querySelector('#output').text = 'Your Dart app is running.';
  BanUI ban = new BanUI();
}
